defmodule Portal do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the Ecto repository
      supervisor(Portal.Repo, []),
      # Start the endpoint when the application starts
      supervisor(Portal.Endpoint, []),
      # Here you could define other workers and supervisors as children
      # worker(Portal.Worker, [arg1, arg2, arg3]),
      supervisor(Portal.Redis, []),

      # authorization cache
      {ConCache, [
        name: :auth_cache,
        global_ttl: :timer.minutes(15),
        ttl_check_interval: :timer.minutes(1)]}
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Portal.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Portal.Endpoint.config_change(changed, removed)
    :ok
  end
end
