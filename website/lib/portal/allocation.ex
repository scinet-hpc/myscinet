defmodule Portal.Allocation do
  require Portal.Redis

  def get_accounts_for_user(username) do
    {:ok, [accounts] } = Portal.Redis.pipeline([["GET", "allocation:accounts:"<>username]])
    case accounts do
      nil -> [""]
      _ ->
        accounts|>String.split
    end
  end

  def get_account_priority(id) do
    alloc = "allocation:" <> id
    Portal.Redis.hgetall([alloc, alloc <> ":sshare"]) # [allocation, sshare]
  end

end
