defmodule Portal.Jobscript do
  use Ecto.Schema

  @primary_key false
  schema "jobscript" do
    field :jobid, :string
    field :jobscript, :string
  end
end
