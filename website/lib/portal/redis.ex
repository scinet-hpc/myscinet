defmodule Portal.Redis do
  use Supervisor

  @redis_connection_params Application.get_env(:portal, Portal.Redis)

  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def init([]) do
    pool_opts = [
      name: {:local, :redix_poolboy},
      worker_module: Redix,
      size: 10,
      max_overflow: 5,
    ]

    children = [
      :poolboy.child_spec(:redix_poolboy, pool_opts, @redis_connection_params)
    ]

    supervise(children, strategy: :one_for_one, name: __MODULE__)
  end

  def command(command) do
    :poolboy.transaction(:redix_poolboy, &Redix.command(&1, command))
  end

  def pipeline(commands) do
    :poolboy.transaction(:redix_poolboy, &Redix.pipeline(&1, commands))
  end

  # convert ["a", "b", ...] -> [a: "b", ...]
  defp pairup(pairs, [k,v|rest]), do:
    pairup([{String.to_atom(k),v}|pairs], rest)
  defp pairup(pairs, _), do: Enum.reverse pairs
  defp pairup(list), do: pairup([], list)

  def hgetall(keys) do
    cmds = for key <- keys, do: ["HGETALL", key]
    {:ok, results} = pipeline(cmds)
    Enum.map results, &pairup/1
  end

end
