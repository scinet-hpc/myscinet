defmodule Portal.Testjob do
  use Ecto.Schema

  @primary_key false
  schema "testjob" do
    field :tjid,     :string
    field :runmonth, :string
    field :nodename, :string
    field :ior_time, :float
    field :ior_write, :float
    field :ior_read, :float
    field :hpl_time, :float
    field :hpl_gflops, :float
    field :hpcg_time, :float
    field :hpcg_gflops, :float
    field :minidft_time, :float
    field :minidft_cpu, :float
  end
end
