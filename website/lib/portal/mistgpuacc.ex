defmodule Portal.Mistgpuacc do
  use Ecto.Schema

  @primary_key false
  schema "mistgpuacc" do
    field :nodejobid, :string
    field :jobid, :string
    field :node, :string
    field :gpuacc, :string
  end
end
