defmodule Portal.Storage do
  require Portal.Redis

  @filesystem_redis_name [home: "home",
        scratch: "scratch",
        project: "project",
        archive: "archive"]

  @filesystems for {key,_} <- @filesystem_redis_name, do: key

  defp get_storage_info(keys, queries) do
    results = for result <- Portal.Redis.hgetall(queries) do
      for {k,v} <- result, do: {k, String.to_integer v}
    end
    Enum.filter Enum.zip(keys, results), fn
      {_, []} -> false
      _ -> true
    end
  end

  defp redis_filesystem_user(fs, user) do
    "filesystem:" <> Keyword.get(@filesystem_redis_name, fs) <> ":user:" <> user
  end

  defp redis_filesystem_group(fs, grp) do
    "filesystem:" <> Keyword.get(@filesystem_redis_name, fs) <> ":group:" <> grp
  end

  def storage_for_user(user) do
    queries = for fs <- @filesystems, do: redis_filesystem_user(fs, user)
    get_storage_info(@filesystems, queries)
  end

  def storage_for_group(group) do
    queries = for fs <- @filesystems, do: redis_filesystem_group(fs, group)
    get_storage_info(@filesystems, queries)
  end

  def breakdown_by_users(filesystem, users) do
    queries = for user <- users do
      redis_filesystem_user(String.to_atom(filesystem), user)
    end
    Enum.filter get_storage_info(users, queries),
      fn ({_,v}) -> v[:space] > 0 end
  end

end
