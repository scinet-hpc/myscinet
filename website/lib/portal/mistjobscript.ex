defmodule Portal.Mistjobscript do
  use Ecto.Schema

  @primary_key false
  schema "mistjobscript" do
    field :jobid, :string
    field :jobscript, :string
  end
end
