defmodule Portal.Jobshow do
  use Ecto.Schema

  @primary_key false
  schema "jobshow" do
    field :jobid, :string
    field :jobshow, :string
  end
end
