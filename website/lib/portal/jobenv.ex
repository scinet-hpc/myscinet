defmodule Portal.Jobenv do
  use Ecto.Schema

  @primary_key false
  schema "jobenv" do
    field :jobid, :string
    field :jobenv, :string
  end
end
