defmodule Portal.Mistjobshow do
  use Ecto.Schema

  @primary_key false
  schema "mistjobshow" do
    field :jobid, :string
    field :jobshow, :string
  end
end
