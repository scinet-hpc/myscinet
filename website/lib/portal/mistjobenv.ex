defmodule Portal.Mistjobenv do
  use Ecto.Schema

  @primary_key false
  schema "mistjobenv" do
    field :jobid, :string
    field :jobenv, :string
  end
end
