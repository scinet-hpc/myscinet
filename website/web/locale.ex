defmodule Portal.Locale do

  def get_locale(conn) do
    if conn.assigns[:locale] do
        conn.assigns[:locale]
    else
        "en"
    end
  end

end
