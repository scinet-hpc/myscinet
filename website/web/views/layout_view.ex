defmodule Portal.LayoutView do
  use Portal.Web, :view

  def full_path_with_locale(conn, locale) do
    q = URI.decode_query conn.query_string
    q = Map.put q, "locale", locale
    URI.to_string %URI{path: conn.request_path, query: URI.encode_query(q)}
  end
end
