defmodule Portal.PageView do
  use Portal.Web, :view

  def nice_float_round(num, prec \\ 0)
  def nice_float_round(nil, _), do: nil
  def nice_float_round(num, prec), do: Float.round(num, prec)
end
