defmodule Portal.JobView do
  use Portal.Web, :view

  def job_index_page_path(conn, page) do
    opts = if page == 1 do
      Map.delete conn.query_params, "p"
    else
      Map.merge conn.query_params, %{"p" => page}
    end
    job_path conn, :index, opts
  end

  def hosts(job) do
    String.split(job.exec_host, "+")
      |> Enum.map(&(hd String.split(&1, "/")))
      |> MapSet.new
      |> MapSet.to_list
  end

  def split_path(nil), do: nil
  def split_path(path) do
    List.last String.split(path, ":")
  end

  def add_underscore_breaks(s) do
    {:safe, String.replace(s, "_", "<wbr>_")}
  end

  # parse elasticsearch date, e.g., "2016-03-19T19:21:22.000Z"
  defp i2(a, b), do: 10*(a-48) + (b-48)
  defp i4(a, b, c, d), do: 10*(10*(10*(a-48) + (b-48)) + (c-48)) + (d-48)
  def parsedate(d) do
    << y1, y2, y3, y4, ?-, mo1, mo2, ?-, d1, d2, ?T,
       h1, h2, ?:, m1, m2, ?:, s1, s2, _::binary >> = d
    { { i4(y1,y2,y3,y4), i2(mo1,mo2), i2(d1,d2) },
      { i2(h1,h2), i2(m1,m2), i2(s1,s2) } }
  end

  defp tm2sec([t]) do
    case Regex.match?(~r/\.[0-9][0-9][0-9]/,t) do
      true ->
        <<m1, m2, ?:, s1, s2, _::binary >> = t
        i2(m1,m2)*60 + i2(s1,s2)
      false ->
        <<h1, h2, ?:, m1, m2, ?:, s1, s2 >> = t
        i2(h1,h2)*60*60 + i2(m1,m2)*60 + i2(s1,s2)
    end
  end

  defp day2sec(t) do
    String.to_integer(t) * 24 * 60 * 60
  end

  defp to_sec(t) do
    [head|tail] = String.split(t,"-")
    case tail do
      [] -> tm2sec([head])
      _ -> day2sec(head) + tm2sec(tail)
    end
  end

  def timediff(d1, d2) do
    #:calendar.datetime_to_gregorian_seconds(parsedate(d1)) -
    #:calendar.datetime_to_gregorian_seconds(parsedate(d2))
    NaiveDateTime.diff(d1,d2)
  end

  def prettydate(d) do
    << _::binary-size(5), a::binary-size(11), _::binary >> = d
    String.replace(a, "T", " ")
  end

  def pretty_interval(nil), do: nil
  def pretty_interval(x) do
    s = rem(x, 60)
    x = div(x, 60)
    m = rem(x, 60)
    h = div(x, 60)
    m = m + min(s, 1)
    if h > 0 do
      "#{h}h " <> String.pad_leading("#{m}",2,"0") <> "m"
    else
      "#{m}m"
    end
  end

  def pretty_interval_comparison(a, b) do
    if a == 0 && b == 0 do
      "0m out of 0m (0%)"
    else
      p = Float.round(100*a/b, 1)
      pretty_interval(a) <> " out of " <> pretty_interval(b) <> " (#{p}%)"
    end
  end

  def insert_at(string, position, insert) do
    case String.split_at(string, position) do
      {"",_} -> string
      {_,""} -> string
      {a, b} -> a <> insert <> b
    end
  end

  def cpu_used(job) do
    case job.totalcpu do
      "-"        -> "N/A"
      "00:00:00" -> "0%"
      _ ->
        # XXX: the 2 is for hyperthreading
        "#{Float.round(2 * 100 * to_sec(job.totalcpu)/to_sec(job.cputime), 1)}%"
    end
  end

  def pretty_mem(m) do
    case String.split(m, "K") do
      [n,""] -> "#{Float.round(String.to_integer(n)/(1024.0*1024.0), 1)} " <> gettext("GiB")
      _ -> m
    end
  end
end
