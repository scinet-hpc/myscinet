defmodule Portal.StorageView do
  use Portal.Web, :view

  def niceunits0(x, r, d, u, []), do: {x, r, d, u}
  def niceunits0(x, r, d, u, [{d2,u2}|rest]) do
    x2 = div(x, d2)
    if x2 == 0 do
      {x, r, d, u}
    else
      r2 = rem(x, d2)
      niceunits0(x2, r+r2*d, d*d2, u2, rest)
    end
  end

  def niceunits(x, u, rest) do
    {x,r,d,u} = niceunits0(x, 0, 1, u, rest)
    if r == 0 do
      {"#{x}", u}
    else
      {"#{niceround x+r/d}", u}
    end
  end

  def niceround(x), do: if x < 10, do: round(10*x)/10, else: round(x)

  def nicekbytes(x) do
    {a,b} = niceunits(x, gettext("kiB"), [{1024,gettext("MiB")}, {1024,gettext("GiB")}, {1024,gettext("TiB")}])
    "#{a} #{b}"
  end

  def nicemetric(x) do
    {a,b} = niceunits(x, "", [{1000,"k"}, {1000,"M"}, {1000,"G"}])
    "#{a}#{b}"
  end

  def nicequota2(x) do
    if x > 0 do
      nicekbytes(x)
    else
      ""
    end
  end

  def nicequota10(x) do
    if x > 0 do
      nicemetric(x)
    else
      ""
    end
  end

  def progressbar(_, 0), do: ""
  def progressbar(a, b) do
    valuemin = 0
    valuemax = 100
    valuenow = round 100*a/b
    width = min(valuenow, 100)
    class = cond do
      valuenow > 90 -> "progress-bar-danger"
      valuenow > 75 -> "progress-bar-warning"
      true -> "progress-bar-info"
    end
    label = if valuenow == 0 && a > 0, do: "<1%", else: "#{valuenow}%"
    {l,r} = if valuenow < 20, do: {"",label}, else: {label,""}
    {:safe, ~s(<div class="progress"><div class="progress-bar #{class}" role="progressbar" aria-valuenow="#{valuenow}" aria-valuemin="#{valuemin}" aria-valuemax="#{valuemax}" style="width:#{width}%">#{l}</div><span class="progress-val-outer">#{r}</span></div>)}
  end
end
