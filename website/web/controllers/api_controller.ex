defmodule Portal.ApiController do
  use Portal.Web, :controller
  import Ecto.Query

  NimbleCSV.define(TSV, separator: "\t")

  @fields Portal.Util.__schema__(:fields) -- [:acores, :etime, :jobid, :jobname, :jobstate, :nnodes, :userid]

  def utilization(conn, %{"jobid" => jobid}) do
    username = conn.assigns.current_user.username
    case ConCache.get(:auth_cache, {jobid, username}) do
      true ->
        query = from Portal.Util,
            select: ^@fields,
            where: [jobid: ^jobid],
            order_by: [asc: :time]
        rows = Portal.Repo.all(query)
        vals = for row <- rows, do: for field <- @fields, do: Map.get(row, field)
        conn
          |> put_resp_content_type("text/tab-separated-values")
          |> send_resp(200, TSV.dump_to_iodata([@fields|vals]))
      _ ->
        conn |> send_resp(404, nil)
    end
  end

end
