defmodule Portal.MistjobController do
  use Portal.Web, :controller
  require Logger

  import Ecto.Query

  plug :setnav

  defp setnav(conn, _) do
    assign conn, :nav, "mistjobs"
  end

  defp query_authz(query, conn) do
    username = conn.assigns.current_user.username
    groups = conn.assigns.current_user.groups
    cond do
      is_staff_user?(conn) ->
        # staff can see all jobs
        query
      username in groups ->
        # PIs can see their group's jobs
        query |> where(groupname: ^username )
      true ->
        # everyone else can only see their own jobs
        query |> where(username: ^username )
    end
  end

  defp query_search(query, q) do
    if q do
      if Regex.match?(~r/\ASEARCHSCRIPT /, q) do
        # search job scripts
        "SEARCHSCRIPT " <> q2 = q
        qlike = "%" <> q2 <> "%"
        joblist = Portal.Mistjobscript
          |> where([u], like(u.jobscript, ^qlike))
          |> select([u],[u.jobid]) |> Portal.Repo.all |> List.flatten
        query
          |> where([u], u.jobid == fragment("ANY(?)", ^joblist))
      else
        # search job metadata
        qlike = "%" <> q <> "%"
        query
          |> where([j], like(j.account,  ^qlike)
                     or like(j.jobid,    ^qlike)
                     or like(j.jobname,  ^qlike)
                     or like(j.state,    ^qlike)
                     or like(j.username, ^qlike))
      end
    else
      query
    end
  end

  def index(conn, params) do
    n = String.to_integer Map.get(params, "n", "10")
    p = String.to_integer Map.get(params, "p", "1")
    q = params["q"]

    o = n*(p - 1)

    query = Portal.Mistjobsum
      |> query_authz(conn)
      |> query_search(q)
      |> limit(^n)
      |> offset(^o)
      |> order_by(desc: :endtime)
      |> select([:account, :elapsed, :endtime, :jobid, :jobname, :nnodes, :username, :state])

    jobs = Portal.Repo.all(query)

    conn
      |> assign(:jobs, jobs)
      |> render("index.html", page: p)
  end

  def show(conn, %{"id" => id}) do
    query = Portal.Mistjobsum
      |> where(jobid: ^id)
    job = Portal.Repo.one(query)

    query2 = Portal.Mistgpuacc
      |> where(jobid: ^id)
      |> select([:nodejobid, :gpuacc])
    gpuacc = Portal.Repo.all(query2)

    case job do
      nil ->
        conn
          |> put_status(:not_found)
          |> text("not found or not permitted")
      _ ->
        script = Portal.Repo.one(from Portal.Mistjobscript, where: [jobid: ^id])
        env = Portal.Repo.one(from Portal.Mistjobenv, where: [jobid: ^id])
        conn
          |> assign(:job, job)
          |> assign(:jobgpuacc, gpuacc)
          |> assign(:jobscript, script)
          |> assign(:jobenv, env)
          |> render("show.html")
    end
  end
end
