defmodule Portal.PageController do
  use Portal.Web, :controller
  require Portal.Redis

  defp get(item) do
    for result <- Portal.Redis.hgetall([item]) do
      for {k,v} <- result do
        {v, _} = Float.parse v
        {k, v}
      end
    end
  end

  def index(conn, _) do
    [raw] = for result <- Portal.Redis.hgetall(["nia:queue:cluster"]) do
      for {k,v} <- result, do: {k, String.to_integer v}
    end

    {:ok, [totalnodes] } = Portal.Redis.pipeline([["GET", "niagara:totalnodes"]])
    tnodes = case totalnodes do
      nil ->
        2016
      _ ->
        {n, ""} = Integer.parse totalnodes
        n
    end

    nia = case raw do
      [] -> nil
      _ ->
        active = raw[:nodesRunning]
        [
          time: raw[:time],
          load: 100*active/tnodes,
          active_count: raw[:active_count],
          queued_count: raw[:queued_count],
          active_cluster_hrs: raw[:active_procsecs]/(3600*active),
          queued_cluster_hrs: raw[:queued_procsecs]/(3600*active)
        ]
    end

    [filesystems] = get("filesystems")
    [loginnodes] = get("loginnodes")
    {:ok, jobwalltime } = Portal.Redis.pipeline([["GET", "jobwalltime"]])
    {:ok, [nodefree] } = Portal.Redis.pipeline([["GET", "nodefree"]])
    {:ok, [jobfree] } = Portal.Redis.pipeline([["GET", "jobfree"]])

    conn
      |> assign(:nia, nia)
      |> assign(:filesystems, filesystems)
      |> assign(:loginnodes, loginnodes)
      |> assign(:jobwalltime, jobwalltime)
      |> assign(:nodefree, nodefree)
      |> assign(:jobfree, jobfree)
      |> assign(:tnodes, tnodes)
      |> render("index.html")
  end

end
