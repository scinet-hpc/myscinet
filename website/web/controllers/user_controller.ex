defmodule Portal.UserController do
  use Portal.Web, :controller
  require Portal.LDAP
  require Portal.Storage
  import Ecto.Query

  plug :setnav

  defp setnav(conn, _) do
    assign conn, :nav, "users"
  end

  def index(conn, %{"q" => q}) do
    users = Portal.LDAP.user_search(q)
    conn
      |> assign(:users, users)
      |> render("index.html")
  end
  def index(conn, _), do: render conn, "index.html"

  def show(conn, %{"id" => id}) do

    try do
        info = Portal.LDAP.user_info(id)

        query = Portal.Jobsum
          |> where(username: ^id)
          |> limit(10)
          |> order_by(desc: :endtime)
          |> select([:account, :elapsed, :endtime, :jobid, :jobname, :nnodes, :username, :state])
        jobs = Portal.Repo.all(query)

        allocations = Portal.Allocation.get_accounts_for_user(id)

        storage = Portal.Storage.storage_for_user(id)

        conn
          |> assign(:user, info)
          |> assign(:jobs, jobs)
          |> assign(:allocations, allocations)
          |> assign(:storage, storage)
          |> render("show.html")
    rescue
        _ -> render(conn, Portal.ErrorView, "404.html")
    end
  end

end
