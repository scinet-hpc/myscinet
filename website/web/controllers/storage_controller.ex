defmodule Portal.StorageController do
  use Portal.Web, :controller
  require Portal.LDAP
  require Portal.Storage

  plug :setnav

  defp setnav(conn, _) do
    assign conn, :nav, "storage"
  end

  def index(conn, _params) do
    user = conn.assigns.current_user.username
    storage = Portal.Storage.storage_for_user(user)
    conn
      |> assign(:links, nil)
      |> assign(:group, nil)
      |> assign(:groups, conn.assigns.current_user.groups)
      |> assign(:storage, storage)
      |> render("table.html")
  end

  defp has_access?(conn, group) do
    groups = conn.assigns.current_user.groups
    group in groups or is_staff_user?(conn)
  end

  defp not_found(conn) do
    conn
      |> put_status(404)
      |> render(Portal.ErrorView, "404.html")
  end

  def show(conn, %{"id" => group}) do
    storage = Portal.Storage.storage_for_group(group)
    links = for {fs,_} <- storage do
      {fs, storage_path(conn, :breakdown, group, fs)}
    end
    if has_access?(conn, group) do
      conn
        |> assign(:links, links)
        |> assign(:group, group)
        |> assign(:groups, conn.assigns.current_user.groups)
        |> assign(:storage, storage)
        |> render("table.html")
    else
      not_found conn
    end
  end

  def breakdown(conn, %{"group" => group, "filesystem" => filesystem}) do
    users = Portal.LDAP.group_members(group)
    results = Portal.Storage.breakdown_by_users(filesystem, users)
    if has_access?(conn, group) do
      conn
        |> assign(:group, group)
        |> assign(:filesystem, filesystem)
        |> assign(:users, results)
        |> render("breakdown.html")
    else
      not_found conn
    end
  end
end
