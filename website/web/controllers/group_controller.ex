defmodule Portal.GroupController do
  use Portal.Web, :controller
  require Portal.Storage
  import Ecto.Query

  def show(conn, %{"id" => id}) do

    query = Portal.Jobsum
      |> where(groupname: ^id)
      |> limit(10)
      |> order_by(desc: :endtime)
      |> select([:account, :elapsed, :endtime, :jobid, :jobname, :nnodes, :username, :state])
    jobs = Portal.Repo.all(query)

    storage = Portal.Storage.storage_for_group(id)

    conn
      |> assign(:group, id)
      |> assign(:jobs, jobs)
      |> assign(:storage, storage)
      |> render("show.html")
  end

end
