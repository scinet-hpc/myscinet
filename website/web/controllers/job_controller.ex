defmodule Portal.JobController do
  use Portal.Web, :controller
  require Logger

  import Ecto.Query

  plug :setnav

  defp setnav(conn, _) do
    assign conn, :nav, "jobs"
  end

  defp query_authz(query, conn) do
    username = conn.assigns.current_user.username
    groups = conn.assigns.current_user.groups
    cond do
      is_staff_user?(conn) ->
        # staff can see all jobs
        query
      username in groups ->
        # PIs can see their group's jobs
        query |> where(groupname: ^username)
      true ->
        # everyone else can only see their own jobs
        query |> where(username: ^username)
    end
  end

  defp query_search(query, q) do
    if q do
      q = String.trim(q)
      cond do
        Regex.match?(~r/\ASEARCHSCRIPT /, q) ->
          # search job scripts
          "SEARCHSCRIPT " <> q2 = q
          qlike = "%" <> q2 <> "%"
          joblist = Portal.Jobscript
            |> where([u], like(u.jobscript, ^qlike))
            |> select([u],[u.jobid]) |> Portal.Repo.all |> List.flatten
          query
            |> where([u], u.jobid == fragment("ANY(?)", ^joblist))
        Regex.match?(~r/^\d+_?\d*$/, q) ->
          query
            |> where([j], j.jobid == ^q)
        true ->
          # search job metadata
          qlike = "%" <> q <> "%"
          query
            |> where([j], like(j.account,  ^qlike)
                       or like(j.jobname,  ^qlike)
                       or like(j.state,    ^qlike)
                       or like(j.username, ^qlike))
      end
    else
      query
    end
  end

  defp clamp(number, min_, max_) do
    number |> max(min_) |> min(max_)
  end

  def index(conn, params) do
    n = Map.get(params, "n", "0") |> String.to_integer |> clamp(10, 100)
    p = Map.get(params, "p", "0") |> String.to_integer |> clamp(1, 10000)
    q = params["q"]

    o = n*(p - 1)

    query = Portal.Jobsum
      |> query_authz(conn)
      |> query_search(q)
      |> limit(^n)
      |> offset(^o)
      |> order_by(desc: :endtime)
      |> select([:account, :elapsed, :endtime, :jobid, :jobname, :nnodes, :username, :state])

    query = if params["user"] do
      query |> where(username: ^params["user"])
    else
      query
    end

    query = if params["group"] do
      query |> where(groupname: ^params["group"])
    else
      query
    end

    jobs = Portal.Repo.all(query)

    conn
      |> assign(:jobs, jobs)
      |> render("index.html", page: p)
  end

  def show(conn, %{"id" => id}) do
    query = Portal.Jobsum
      |> query_authz(conn)
      |> where(jobid: ^id)

    job = Portal.Repo.one(query)

    case job do
      nil ->
        conn
          |> put_status(:not_found)
          |> text("not found or not permitted")
      _ ->
        script = Portal.Repo.one(from Portal.Jobscript, where: [jobid: ^id])
        env = Portal.Repo.one(from Portal.Jobenv, where: [jobid: ^id])
        ConCache.put(:auth_cache, {id, conn.assigns.current_user.username}, true)
        conn
          |> assign(:job, job)
          |> assign(:jobscript, script)
          |> assign(:jobenv, env)
          |> render("show.html")
    end
  end
end
