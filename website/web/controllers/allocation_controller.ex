defmodule Portal.AllocationController do
  use Portal.Web, :controller
  require Portal.Allocation
  import Ecto.Query

  plug :setnav

  defp setnav(conn, _) do
    assign conn, :nav, "allocations"
  end

  def index(conn, _params) do
    accounts = Portal.Allocation.get_accounts_for_user(conn.assigns.current_user.username)
    conn
      |> assign(:raps, accounts)
      |> render("index.html")
  end

  defp get_priority(conn, id) do
    [allocation, sshare] = Portal.Allocation.get_account_priority(id)
    conn
      |> assign(:nia, allocation)
      |> assign(:sshare, sshare)
  end

  def get_usage(conn, account) do
    current_time=NaiveDateTime.utc_now
    {:ok, apr1a}=NaiveDateTime.new(current_time.year,4,1,0,0,0)
    {:ok, apr1b}=NaiveDateTime.new(current_time.year-1,4,1,0,0,0)
    rac_start_datetime = if NaiveDateTime.diff(current_time,apr1a) > 0 do
                           apr1a
                         else
                           apr1b
                         end

    query = from j in Portal.Jobsum,
      select: [j.username, fragment("(40.0/(365*24*60*60))*sum(nnodes*extract(epoch from (endtime - start))) as usage"), fragment("count(*)")],
      where: j.account == ^account and j.endtime > ^rac_start_datetime,
      group_by: j.username,
      order_by: [desc: fragment("usage")]

    usage = Portal.Repo.all(query)

    conn
      |> assign(:rac_start_date, NaiveDateTime.to_date(rac_start_datetime))
      |> assign(:usage, usage)
  end

  defp has_access?(conn, account) do
    accounts = Portal.Allocation.get_accounts_for_user(conn.assigns.current_user.username)
    account in accounts or is_staff_user?(conn)
  end

  def show(conn, %{"id" => id}) do
    if has_access?(conn, id) do
      conn
        |> get_priority(id)
        |> get_usage(id)
        |> render("show.html")
    else
      conn
        |> put_status(404)
        |> render(Portal.ErrorView, "404.html")
    end
  end
end
