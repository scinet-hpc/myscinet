defmodule Portal.SessionController do
  use Portal.Web, :controller
  require Portal.LDAP

  def new(conn, _params) do
    render conn, "new.html"
  end

  def about(conn, _params) do
    conn
      |> assign(:nav, "account")
      |> render("about.html")
  end

  defp become_user(conn, info, msg) do
    path = get_session(conn, :redirect_to) || "/"
    conn
      |> delete_session(:redirect_to)
      |> put_session(:current_user, info)
      |> put_flash(:info, msg)
      |> redirect(to: path)
  end

  def create(conn, %{"session" => session_params}) do
    %{"username" => username, "password" => password} = session_params
    case Portal.LDAP.authenticate(username, password) do
      {:ok, info} ->
        conn
          |> become_user(info, gettext("Signed in as %{user}", user: username))

      _ ->
        :timer.sleep(2000)
        conn
          |> put_flash(:error, gettext("Bad username or password"))
          |> redirect(to: session_path(conn, :new))
    end
  end

  def delete(conn, _params) do
    current_user = conn.assigns.current_user
    conn
    |> clear_session
    |> put_flash(:info, gettext("Signed out %{user}", user: current_user.username))
    |> redirect(to: "/")
  end
end
