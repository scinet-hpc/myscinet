defmodule Portal.TestjobController do
  use Portal.Web, :controller
  import Ecto.Query

  plug :setnav

  defp setnav(conn, _) do
    assign conn, :nav, "testjobs"
  end

  def index(conn, _) do
    months = Portal.Testjob
      |> distinct(true)
      |> select([u], u.runmonth)
      |> order_by(desc: :runmonth)
      |> Portal.Repo.all

    assign(conn, :months, months)
      |> render("index.html")
  end

  defp get_tests(conn, id) do
    result = Portal.Testjob
      |> where([u], u.runmonth == ^id)
      |> select([u], %{nodename: u.nodename, ior_time: u.ior_time, ior_write: u.ior_write, ior_read: u.ior_read, hpl_time: u.hpl_time, hpl_gflops: u.hpl_gflops, hpcg_time: u.hpcg_time, hpcg_gflops: u.hpcg_gflops, minidft_time: u.minidft_time, minidft_cpu: u.minidft_cpu})
      |> order_by(asc: :nodename)
      |> Portal.Repo.all
    assign conn, :tests, result
  end

  defp get_tests_node(conn, id) do
    result = Portal.Testjob
      |> limit(5000)
      |> where([u], u.nodename == ^id)
      |> select([u], %{runmonth: u.runmonth, ior_time: u.ior_time, ior_write: u.ior_write, ior_read: u.ior_read, hpl_time: u.hpl_time, hpl_gflops: u.hpl_gflops, hpcg_time: u.hpcg_time, hpcg_gflops: u.hpcg_gflops, minidft_time: u.minidft_time, minidft_cpu: u.minidft_cpu})
      |> order_by(desc: :runmonth)
      |> Portal.Repo.all
    assign conn, :tests, result
  end

  def node(conn, %{"id" => id}) do
    conn
      |> get_tests_node(id)
      |> assign(:title, "Niagara node #{id} performance")
      |> render("node.html")
  end

  def week(conn, %{"id" => id}) do
    conn
      |> get_tests(id)
      |> render("show.html")
  end

  def average(conn, _) do
    query = from t in Portal.Testjob,
      select: %{runmonth: t.runmonth,
        ior_time: fragment("percentile_cont(0.5) within group (order by ?)", t.ior_time),
        ior_read: fragment("percentile_cont(0.5) within group (order by ?)", t.ior_read),
        ior_write: fragment("percentile_cont(0.5) within group (order by ?)", t.ior_write),
        hpl_gflops: fragment("percentile_cont(0.5) within group (order by ?)", t.hpl_gflops),
        hpl_time: fragment("percentile_cont(0.5) within group (order by ?)", t.hpl_time),
        hpcg_gflops: fragment("percentile_cont(0.5) within group (order by ?)", t.hpcg_gflops),
        hpcg_time: fragment("percentile_cont(0.5) within group (order by ?)", t.hpcg_time),
        minidft_cpu: fragment("percentile_cont(0.5) within group (order by ?)", t.minidft_cpu),
        minidft_time: fragment("percentile_cont(0.5) within group (order by ?)", t.minidft_time),
      },
      where: t.ior_time > ^0,
      order_by: [desc: :runmonth],
      group_by: t.runmonth
    data = Portal.Repo.all(query)
    conn
      |> assign(:title, "Niagara median single-node performance")
      |> assign(:tests, data)
      |> render("node.html")
  end

end
