defmodule Portal.Router do
  use Portal.Web, :router
  import Portal.Permissions

  defp assign_if_in_session(conn, key) do
    if conn.assigns[key] do
      conn
    else
      val = get_session(conn, key)
      if val do
        assign conn, key, val
      else
        conn
      end
    end
  end

  defp get_locale(%{params: %{"locale" => locale}}), do: locale
  defp get_locale(%{cookies: %{"locale" => locale}}), do: locale
  defp get_locale(conn) do
    case get_req_header(conn, "accept-language") do
      ["fr" <> _] -> "fr"
      _ -> nil
    end
  end

  defp fetch_locale(conn, _) do
    locale = get_locale(conn)
    if locale do
      Gettext.put_locale Portal.Gettext, locale
      put_session conn, :locale, locale
    else
      conn
    end
  end

  defp remember_locale(%{params: %{"locale" => locale}} = conn, _) do
    put_resp_cookie conn, "locale", locale
  end
  defp remember_locale(conn, _), do: conn

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_locale
    plug :remember_locale
    plug :assign_if_in_session, :current_user
    plug :assign_if_in_session, :locale
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :assign_if_in_session, :current_user
  end

  defp full_path(conn) do
    URI.to_string %URI{path: conn.request_path, query: conn.query_string}
  end

  defp authenticate(conn, _) do
    if conn.assigns[:current_user] do
      conn
    else
      conn
        |> put_session(:redirect_to, full_path(conn))
        |> redirect(to: "/login")
        |> halt
    end
  end

  pipeline :authenticated do
    plug :authenticate
  end

  defp is_staff_user(conn, _) do
    if is_staff_user?(conn) do
      conn
    else
      conn
        |> put_status(404)
        |> render(Portal.ErrorView, "404.html")
        |> halt
    end
  end

  pipeline :staff do
    plug :is_staff_user
  end

  defp is_superuser(conn, _) do
    if Portal.Permissions.is_superuser?(conn) do
      conn
    else
      conn
        |> put_status(404)
        |> render(Portal.ErrorView, "404.html")
        |> halt
    end
  end

  pipeline :superuser do
    plug :is_superuser
  end

  scope "/", Portal do
    pipe_through :browser

    get       "/",        PageController, :index
    get       "/login",   SessionController, :new
    post      "/login",   SessionController, :create
  end

  scope "/", Portal do
    pipe_through [:browser, :authenticated]

    get       "/account",     SessionController, :about
    resources "/allocations", AllocationController, only: [:index, :show]
    resources "/jobs/niagara", JobController, only: [:index, :show]
    resources "/jobs/mist",    MistjobController, only: [:index, :show]
    post      "/logout",      SessionController, :delete
    resources "/storage",     StorageController, only: [:index, :show]
    get "/storage/:group/:filesystem", StorageController, :breakdown
  end

  scope "/", Portal do
    pipe_through [:browser, :authenticated, :staff]

    resources "/users", UserController, only: [:index, :show]
    resources "/groups", GroupController, only: [:show]
  end

  scope "/", Portal do
    pipe_through [:browser, :authenticated, :superuser]

    get "/testjobs",                  TestjobController, :index
    get "/testjobs/niagara/average",  TestjobController, :average
    get "/testjobs/niagara/node/:id", TestjobController, :node
    get "/testjobs/niagara/week/:id", TestjobController, :week
  end

  scope "/api/v1", Portal do
    pipe_through [:api, :authenticated]

    get "/jobs/niagara/:jobid/utilization.tsv", ApiController, :utilization
  end
end
