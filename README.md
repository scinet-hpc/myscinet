# my.scinet: SciNet user portal

This is the user web portal for the SciNet supercomputing centre.
Shows information about:

- jobs
- storage
- group allocations
- overall cluster stats

The website is an [Elixir/Phoenix](https://phoenixframework.org/) app.

## Configuration

Add credentials to `website/config/secret.ex`:

```
config :portal, Portal.LDAP,
  hosts: ["hostname1","hostname2"],
  user_base: "ou=xxx,dc=xxx,dc=xxx",
  group_base: "ou=xxx,dc=xxx,dc=xxx",
  bind_dn: "cn=xxx,dc=xxx,dc=xxx",
  bind_pw: "xxx"

config :portal, Portal.Redis,
  host: "xxx",
  password: "xxx"

config :portal, Portal.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "xxx",
  username: "xxx",
  password: "xxx",
  hostname: "xxx"
```

## Databases

### LDAP

User accounts are stored in LDAP.

### SQL

Job info and node utilization is stored in a SQL database
(currently [TimescaleDB](https://www.timescale.com/)/Postgres).
Schemas can be found in the following files:

```
website/
  lib/
    portal/
      jobenv.ex       # job environment
      jobscript.ex    # job scripts
      jobsum.ex       # job summary/metadata
      util.ex         # node utilization
```

### Redis

Information about user/group allocations & storage is stored in Redis.

The following keys are used:

| Key | Type | Description |
| --- | ---- | ----------- |
| `allocation:accounts:<username>` | string | space-separated list of accounts |
| `allocation:<account>` | hash | see below |
| `allocation:<account>:sshare` | hash | see below |
| `filesystem:<fsname>:user:<username>` | hash | see below |
| `filesystem:<fsname>:group:<groupname>` | hash | see below |

On niagara, `<fsname>` is one of `home`, `scratch`, `project`, or `archive`.

#### Key `allocation:<account>`

A hash with the following keys:

| Key | Description |
| --- | ----------- |
| `cores` | size of the allocation, in cores |
| `percent_used` | how much has been used recently |

#### Key `allocation:<account>:sshare`

A hash with the following keys:

| Key | Description |
| --- | ----------- |
| `<username>` | recent usage by this user, as a fraction of total usage |

#### Key `filesystem:<fsname>:user:<username>` / `filesystem:<fsname>:group:<groupname>`

A hash with the following keys:

| Key | Description |
| --- | ----------- |
| `space` | storage used (bytes) |
| `space_quota` | storage quota (bytes) |
| `files` | number of files |
| `file_quota` | file number quota |

